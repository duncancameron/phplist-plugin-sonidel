<?php
/**
 * Sonidel Plugin for phplist.
 *
 * This file is a part of Sonidel Plugin.
 *
 * This plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @category  phplist
 *
 * @author    Duncan Cameron
 * @copyright 2023 Duncan Cameron
 * @license   http://www.gnu.org/licenses/gpl.html GNU General Public License, Version 3
 */
use Pdp\Domain;
use Pdp\Rules;
use phpList\plugin\Common\Cache;

class Sonidel extends phplistPlugin
{
    const VERSION_FILE = 'version.txt';
    const PUBLIC_SUFFIX_LIST_URL = 'https://publicsuffix.org/list/public_suffix_list.dat';

    public $name = 'Sonidel Plugin';
    public $authors = 'Duncan Cameron';
    public $description = 'Sonidel plugin';
    public $coderoot = __DIR__ . '/' . __CLASS__ . '/';

    private $publicSuffixList;

    public function __construct()
    {
        $this->version = file_get_contents($this->coderoot . self::VERSION_FILE);
        parent::__construct();
    }

    public function dependencyCheck()
    {
        global $plugins;

        return [
            'Common Plugin v3.31.0 or later installed' => (
                phpListPlugin::isEnabled('CommonPlugin')
                && version_compare($plugins['CommonPlugin']->version, '3.31.0') >= 0
            ),
        ];
    }

    public function processQueueStart()
    {
        $cache = Cache::instance();

        if (($suffixListData = $cache->get('public_suffix_list')) === null) {
            $suffixListData = fetchUrlDirect(self::PUBLIC_SUFFIX_LIST_URL);
            $cache->set('public_suffix_list', $suffixListData, 3600 * 24 * 7);
        }
        $this->publicSuffixList = Rules::fromString($suffixListData);
    }

    public function throttleDomainMap($domain)
    {
        $domain = Domain::fromIDNA2008($domain);
        $resolvedDomainName = $this->publicSuffixList->resolve($domain);
        $registeredDomain = $resolvedDomainName->registrableDomain()->toString();

        return $registeredDomain == $domain ? false : $registeredDomain;
    }
}
