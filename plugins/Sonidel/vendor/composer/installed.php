<?php return array(
    'root' => array(
        'name' => 'bramley/phplist-plugin-sonidel',
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'reference' => NULL,
        'type' => 'phplist-plugin',
        'install_path' => __DIR__ . '/../../../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'bramley/phplist-plugin-sonidel' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'reference' => NULL,
            'type' => 'phplist-plugin',
            'install_path' => __DIR__ . '/../../../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'jeremykendall/php-domain-parser' => array(
            'pretty_version' => '6.3.0',
            'version' => '6.3.0.0',
            'reference' => '34c7177cf322771fc241b9dcc8eda89f41a0149c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../jeremykendall/php-domain-parser',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
